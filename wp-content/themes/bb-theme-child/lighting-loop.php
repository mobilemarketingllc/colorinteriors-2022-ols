<div class="product-plp-grid product-grid swatch" itemscope itemtype="http://schema.org/ItemList">
    <div class="row product-row">
        <?php 
            global $wpdb;
            $col_class = 'col-lg-4 col-md-4 col-sm-6 ';
            $K = 1;

            while ( $prod_list->have_posts() ): $prod_list->the_post(); 
            
                $meta_values = get_post_meta( get_the_ID() );
        ?>
                <div class="<?php echo $col_class; ?>">    
                    <div class="fl-post-grid-post" itemprop="itemListElement" itemscope  itemtype="http://schema.org/ListItem">

                        <meta itemprop="position" content="<?php echo $K; echo $prod_list->ID;?>" />
                        <?php if(@$meta_values['product_image'][0]) { ?>
                            <div class="fl-post-grid-image">
                                <a itemprop="url" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                <?php 
                                    $image = $meta_values['product_image'][0];
                                    ?>
                                <img class="list-pro-image" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
                                
                                </a>
                            </div>
                        <?php } else { ?>
                            <div class="fl-post-grid-image">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                    <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                                </a>
                            </div>

                        <?php } ?>
    
                        <div class="fl-post-grid-text product-grid btn-grey">
                            <h3>
                                <span> <?php  echo $meta_values['brand'][0];  ?></span>
                            </h3>
                            <h4>
                                <span> <?php  echo $meta_values['standard_subcategory'][0];  ?></span>
                            </h4>
                            <?php if($meta_values['price'][0]){ ?>
                            <h5>
                                <span> <?php  echo "$".$meta_values['price'][0];  ?></span>
                            </h5>
                            <?php } ?>
                            <a class="fl-button plp_box_btn" href="<?php the_permalink(); ?>">VIEW PRODUCT</a><br>
                        </div>
                    </div>
                </div>
        <?php  
            $K++; 
            endwhile; 
            wp_reset_postdata();
        ?>
    </div>
</div>
