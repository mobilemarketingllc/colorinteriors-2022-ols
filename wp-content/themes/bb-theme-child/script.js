var $ = jQuery;
jQuery(document).ready(function() {

    var mystore_loc = jQuery(".header_location_name").html();
    jQuery("#input_29_32").val(mystore_loc);
    console.log(mystore_loc);
    console.log('ready');
    jQuery.ajax({
       type: "POST",
       url: "/wp-admin/admin-ajax.php",
       data: 'action=get_storelisting',
       dataType: 'JSON',
       success: function(response) {
          
               var posts = JSON.parse(JSON.stringify(response));
               var header_data = posts.header;
               var header_phone = posts.phone;
               var header_telphone = 'tel:'+posts.phone;
               var list_data = posts.list;

               var mystore_loc =  jQuery.cookie("preferred_storename");
               jQuery(".header_location_name").html(header_data); 
               
               jQuery(".phone .mobile_phone").html(header_phone);              
               jQuery(".phone a.mobile_phone").attr("href", header_telphone); 
				
		   	   jQuery(".mobile a").html(header_phone);              
               jQuery(".header_store .mobile a").attr("href", header_telphone); 
				
               jQuery("#ajaxstorelisting #storeLocation > .content").html(list_data);

       }
   });

});

jQuery(document).on('click', '.choose_location', function() {
 
   var mystore = jQuery(this).attr("data-id");
   var distance = jQuery(this).attr("data-distance");
   var storename = jQuery(this).attr("data-storename");
	
	var storePhone = jQuery(this).attr("data-phone");
	
   jQuery.cookie("preferred_store", null, { path: '/' });
   jQuery.cookie("preferred_distance", null, { path: '/' });
   jQuery.cookie("preferred_storename", null, { path: '/' });
	
	console.log(jQuery(this).attr("data-id"));
	
   jQuery.ajax({
       type: "POST",
       url: "/wp-admin/admin-ajax.php",
       data: 'action=choose_location&store_id=' + jQuery(this).attr("data-id") + '&distance=' + jQuery(this).attr("data-distance") + '&phone=' +jQuery(this).attr("data-phone"),

       success: function(data) {

           //jQuery(".header_store .contentFlyer").html(data);
           jQuery.cookie("preferred_store", mystore, { expires: 1, path: '/' });
           jQuery.cookie("preferred_distance", distance, { expires: 1, path: '/' });
           jQuery.cookie("preferred_storename", storename, { expires: 1, path: '/' });
			
		   
           jQuery(".header_store .contentFlyer .mobile_phone").html(storePhone);
			jQuery(".header_store .contentFlyer .mobile_phone").attr("href", "tel:"+ storePhone + "");
			jQuery(".header_store .contentFlyer .header_location_name").html(storename);

		   
		   
            jQuery("#input_29_32").val(storename);
			
		   if(jQuery(".populate-store select").length > 0 ){
				var mystore_loc =  jQuery.cookie("preferred_storename");
			   var currentlocation = '';
				if( mystore_loc == "Color Interiors Conroe" ){
				   currentlocation = 'Conroe, Texas';
				 }else if (mystore_loc == "Color Interiors Magnolia") {
					currentlocation = 'Magnolia, Texas';	   
				}else if (mystore_loc == "Color Interiors The Woodlands") {
					currentlocation = 'Woodlands,Texas';	   
				}
				jQuery(".populate-store select").val(currentlocation);		
			}	
		   
		   jQuery( "span.uabb-offcanvas-close" ).trigger( "click" );
          // jQuery(".uabb-offcanvas-close").trigger("click");
	setTimeout(addFlyerEvent, 1000);	

		
          
       }
   });

});

//flyer event assign function
function addFlyerEvent() {
   
}
jQuery(document).ready(function() {

    var mystore_loc =  jQuery.cookie("preferred_storename");
    console.log( "sd" + mystore_loc);
	var currentlocation = '';
	if( mystore_loc == "Color Interiors Conroe" ){
	   currentlocation = 'Conroe, Texas';
	 }else if (mystore_loc == "Color Interiors Magnolia") {
		currentlocation = 'Magnolia, Texas';	   
	}else if (mystore_loc == "Color Interiors The Woodlands") {
		currentlocation = 'Woodlands,Texas';	   
	}

    jQuery(".populate-store select").val(currentlocation);

    jQuery('.custom_searchHeader .fl-icon').click(function(){
    	jQuery('.custom_searchModule').slideToggle();
    }) 
})
var customSearch = jQuery('.custom_searchHeader .fl-icon , .custom_searchModule');

jQuery(document).mouseup(function (e) {
    if (!customSearch.is(e.target) && customSearch.has(e.target).length === 0 && jQuery('.custom_searchModule').css('display') == 'block') {
        jQuery('.custom_searchModule').slideToggle();
    }
});