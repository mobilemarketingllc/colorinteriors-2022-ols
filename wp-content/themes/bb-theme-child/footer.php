<?php do_action( 'fl_content_close' ); ?>

	</div><!-- .fl-page-content -->
	<?php

	do_action( 'fl_after_content' );

	if ( FLTheme::has_footer() ) :

		?>
	<footer class="fl-page-footer-wrap"<?php FLTheme::print_schema( ' itemscope="itemscope" itemtype="https://schema.org/WPFooter"' ); ?>  role="contentinfo">
		<?php

		do_action( 'fl_footer_wrap_open' );
		do_action( 'fl_before_footer_widgets' );

		FLTheme::footer_widgets();

		do_action( 'fl_after_footer_widgets' );
		do_action( 'fl_before_footer' );

		FLTheme::footer();

		do_action( 'fl_after_footer' );
		do_action( 'fl_footer_wrap_close' );

		?>
	</footer>
	<?php endif; ?>
	<?php do_action( 'fl_page_close' ); ?>
</div><!-- .fl-page -->
<?php

wp_footer();

do_action( 'fl_body_close' );

FLTheme::footer_code();

?>

<?php global $post; 

if($post->ID == '1249050'){
?>
<script>

function firstLoad(){
    
	if(jQuery('.product_color_select2')){
		var selected = jQuery('.product_color_select2 select');	
		var options = jQuery('.product_color_select2 select option');
		document.write('<div id="currentRugValue" style="display: none;"><?php echo get_the_title($_GET['id']);?></div>');
		var currentValue = document.getElementById('currentRugValue').innerHTML;
		options.each(function(){
			if(jQuery(this).attr('value') ===  currentValue){
				selected.val(currentValue);					
			}
		});
	}
	
}

firstLoad();

        
</script>
<?php } ?>

<script>
	$(function () {
		$('.calculateBtn').m2Calculator({
			measureSystem: "Imperial",            
			thirdPartyName: "Color Interiors", 
			thirdPartyEmail: "lauren@colorinteriors.com",  // if showDiagram = false, will send estimate data to this email when user click Email Estimate button
			showCutSheet: false, // if false, will not include cutsheet section in return image
			showDiagram: true,  // if false, will close the popup directly 
			/*  product: {
				type: "Carpet",
				name: "Carpet 1",
				width: "6'0\"",
				length: "150'0\"",
				horiRepeat: "3'0\"",
				vertRepeat: "3'0\"",
				horiDrop: "",
				vertDrop: ""
			},
			*/
			cancel: function () {
				//when user closes the popup without calculation.
			},
			callback: function (data) {
				//json format, include user input, usage and base64image

				var json = JSON.parse(JSON.stringify(data));

				var res = JSON.stringify(data);
				//  $("#callback").html(JSON.stringify(data));
				var json1 = JSON.parse(JSON.stringify(json.input));                      
				var imageName = '';        
				if (!data.input.rooms.length){
					imageName = data.input.stairs[0].name;
				}else{
					imageName = data.input.rooms[0].name;
				}
				
			}
		});
	
	});        
</script>
</body>
</html>
