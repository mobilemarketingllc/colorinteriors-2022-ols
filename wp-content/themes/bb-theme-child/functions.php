<?php

// Defines
define('FL_CHILD_THEME_DIR', get_stylesheet_directory());
define('FL_CHILD_THEME_URL', get_stylesheet_directory_uri());

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action('wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000);

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script("slick", get_stylesheet_directory_uri() . "/resources/slick/slick.min.js", "", "", 1);
    wp_enqueue_script("cookie", get_stylesheet_directory_uri() . "/resources/jquery.cookie.min.js", "", "", 1);
    wp_enqueue_script("m2FlooringCalculator", "https://calculator.measuresquare.com/scripts/jquery-m2FlooringCalculator.js", "", "");
    wp_enqueue_script("child-script", get_stylesheet_directory_uri() . "/script.js", "", "", 1);
});


// Register menus
function register_my_menus()
{
    register_nav_menus(
        array(
            'footer-1' => __('Footer Menu 1'),
            'footer-2' => __('Footer Menu 2'),
            'footer-3' => __('Footer Menu 3'),
            'footer-4' => __('Footer Menu 4'),
            'footer-5' => __('Footer Menu 5'),
            'site-map' => __('Site Map'),
        )
    );
}
add_action('init', 'register_my_menus');

//Facet Title Hook
add_filter('facetwp_shortcode_html', function ($output, $atts) {
    if (isset($atts['facet'])) {
        $output = '<div class="facet-wrap"><strong>' . $atts['title'] . '</strong>' . $output . '</div>';
    }
    return $output;
}, 10, 2);

// Enable shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');


function fr_img($id = 0, $size = "", $url = false, $attr = "")
{

    //Show a theme image
    if (!is_numeric($id) && is_string($id)) {
        $img = get_stylesheet_directory_uri() . "/images/" . $id;
        if (file_exists(to_path($img))) {
            if ($url) {
                return $img;
            }
            return '<img src="' . $img . '" ' . ($attr ? build_attr($attr) : "") . '>';
        }
    }

    //If ID is empty get the current post attachment id
    if (!$id) {
        $id = get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if (is_object($id)) {
        if (!empty($id->ID)) {
            $id = $id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if (get_post_type($id) != "attachment") {
        $id = get_post_thumbnail_id($id);
    }

    if ($id) {
        $image_url = wp_get_attachment_image_url($id, $size);
        if (!$url) {
            //If image is a SVG embed the contents so we can change the color dinamically
            if (substr($image_url, -4, 4) == ".svg") {
                $image_url = str_replace(get_bloginfo("url"), ABSPATH . "/", $image_url);
                $data = file_get_contents($image_url);
                echo strstr($data, "<svg ");
            } else {
                return wp_get_attachment_image($id, $size, 0, $attr);
            }
        } else if ($url) {
            return $image_url;
        }
    }
}

if (@$_GET['keyword'] != '' && @$_GET['brand'] != "") {
    $url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $url = explode('?', $url);
    setcookie('keyword', $_GET['keyword']);
    setcookie('brand', $_GET['brand']);
    wp_redirect($url[0]);
    exit;
} else if (@$_GET['brand'] != "" && @$_GET['keyword'] == '') {
    $url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $url = explode('?', $url);
    setcookie('brand', $_GET['brand']);
    wp_redirect($url[0]);
    exit;
} else if (@$_GET['brand'] == "" && @$_GET['keyword'] != '') {
    $url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $url = explode('?', $url);
    setcookie('keyword', $_GET['keyword']);
    wp_redirect($url[0]);
    exit;
}


// shortcode to show H1 google keyword fields
function new_google_keyword()
{

    if (@$_COOKIE['keyword'] == ""  && @$_COOKIE['brand'] == "") {
        // return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring<h1>';
        return $google_keyword = '<h1 class="googlekeyword">Save up to $1,000 on Flooring *<h1>';
    } else {
        $keyword = $_COOKIE['keyword'];
        $brand = $_COOKIE['brand'];
        //    return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on '.$brand.' '.$keyword.'<h1>';
        return $google_keyword = '<h1 class="googlekeyword">Save up to $1,000 on ' . $brand . ' ' . $keyword . '<h1>';
    }
}
add_shortcode('google_keyword_code', 'new_google_keyword');
add_action('wp_head', 'cookie_gravityform_js');

function cookie_gravityform_js()
{ // break out of php 
?>
    <!-- <script>
	  var brand_val ='<?php //echo $_COOKIE['brand'];
                        ?>';
	  var keyword_val = '<?php //echo $_COOKIE['keyword'];
                            ?>';  
      jQuery(document).ready(function($) {
      jQuery("#input_14_9").val(keyword_val);
      jQuery("#input_14_10").val(brand_val);
    });
  </script> -->
<?php
    setcookie('keyword', '', -3600);
    setcookie('brand', '', -3600);
}

// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head()
{

?>
    <style>
        .googlekeyword {
            text-align: center;
            color: #fff;
            text-transform: capitalize;
            /* font-size:2.5em !important; */
            font-size: 36px !important;
        }
    </style>
<?php
}

function remove_pagelist_css()
{
    wp_dequeue_style('page-list-style');
    wp_dequeue_style('bbhf-style-css');
}
add_action('wp_print_styles', 'remove_pagelist_css', 100);

wp_clear_scheduled_hook('404_redirection_log_cronjob');
wp_clear_scheduled_hook('404_redirection_301_log_cronjob');
if (!wp_next_scheduled('bmg_404_redirection_301_log_cronjob')) {

    wp_schedule_event(time() +  17800, 'daily', 'bmg_404_redirection_301_log_cronjob');
}

add_action('bmg_404_redirection_301_log_cronjob', 'bmg_custom_404_redirect_hook');

//add_action( '404_redirection_log_cronjob', 'custom_404_redirect_hook' ); 


function bmg_check_404($url)
{
    $headers = get_headers($url, 1);
    if ($headers[0] != 'HTTP/1.1 200 OK') {
        return true;
    } else {
        return false;
    }
}

// custom 301 redirects from  404 logs table
function bmg_custom_404_redirect_hook()
{
    global $wpdb;
    write_log('in function');

    $table_redirect = $wpdb->prefix . 'redirection_items';
    $table_name = $wpdb->prefix . "redirection_404";
    $table_group = $wpdb->prefix . 'redirection_groups';

    $data_404 = $wpdb->get_results("SELECT * FROM $table_name");
    $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
    $redirect_group =  $datum[0]->id;

    if ($data_404) {
        foreach ($data_404 as $row_404) {

            if (strpos($row_404->url, 'carpet') !== false && strpos($row_404->url, 'products') !== false && strpos($row_404->url, 'flooring') !== false) {
                write_log($row_404->url);

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = bmg_check_404($url_404);

                if ($checkalready == 0 && $headers_res == 1) {


                    $destination_url_carpet = '/flooring/carpet/products/';
                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url_carpet,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');

                    write_log('carpet 301 added ');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }
            } else if (strpos($row_404->url, 'hardwood') !== false && strpos($row_404->url, 'products') !== false && strpos($row_404->url, 'flooring') !== false) {

                write_log($row_404->url);

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');
                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = bmg_check_404($url_404);

                if ($checkalready == 0 && $headers_res == 1) {


                    $destination_url_hardwood = '/flooring/hardwood/products/';
                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url_hardwood,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                write_log('hardwood 301 added ');
            } else if (strpos($row_404->url, 'laminate') !== false && strpos($row_404->url, 'products') !== false && strpos($row_404->url, 'flooring') !== false) {

                write_log($row_404->url);

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = bmg_check_404($url_404);

                if ($checkalready == 0 && $headers_res == 1) {


                    $destination_url_laminate = '/flooring/laminate/products/';
                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url_laminate,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                write_log('laminate 301 added ');
            } else if ((strpos($row_404->url, 'luxury-vinyl') !== false || strpos($row_404->url, 'vinyl') !== false) && strpos($row_404->url, 'products') !== false && strpos($row_404->url, 'flooring') !== false) {

                write_log($row_404->url);

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = bmg_check_404($url_404);

                if ($checkalready == 0 && $headers_res == 1) {


                    if (strpos($row_404->url, 'luxury-vinyl') !== false) {
                        $destination_url_lvt = '/flooring/vinyl/products/';
                    } elseif (strpos($row_404->url, 'vinyl') !== false) {
                        $destination_url_lvt = '/flooring/vinyl/products/';
                    }

                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url_lvt,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                write_log('luxury-vinyl 301 added ');
            } else if (strpos($row_404->url, 'tile') !== false && strpos($row_404->url, 'products') !== false && strpos($row_404->url, 'flooring') !== false) {

                write_log($row_404->url);

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = bmg_check_404($url_404);

                if ($checkalready == 0 && $headers_res == 1) {

                    $destination_url_tile = '/flooring/tile/products/';
                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url_tile,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                write_log('tile 301 added ');
            }
        }
    }
}

function lighting_product_post_link($post_link, $id = 0)
{
    $post = get_post($id);
    if (is_object($post) && $post->post_type == 'lighting_product') {
        $terms = wp_get_object_terms($post->ID, 'lighting_category');
        if (!empty($terms)) {
            return str_replace('%lighting_category%', $terms[0]->slug, $post_link);
        }
    }
    return $post_link;
}
add_filter('post_type_link', 'lighting_product_post_link', 1, 3);
//add method to register event to WordPress init
add_action('init', 'register_daily_mysql_bin_log_event');

function register_daily_mysql_bin_log_event()
{
    // make sure this event is not scheduled
    if (!wp_next_scheduled('mysql_bin_log_job')) {
        // schedule an event
        wp_schedule_event(time(), 'daily', 'mysql_bin_log_job');
    }
}

add_action('mysql_bin_log_job', 'mysql_bin_log_job_function');
function mysql_bin_log_job_function()
{

    global $wpdb;
    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'";
    $delete_endpoint = $wpdb->get_results($sql_delete);
}
add_filter('auto_update_plugin', '__return_false');



add_filter('gform_pre_render_32', 'populate_product_color');
add_filter('gform_pre_validation_32', 'populate_product_color');
add_filter('gform_pre_submission_filter_32', 'populate_product_color');
add_filter('gform_admin_pre_render_32', 'populate_product_color');

function populate_product_color($form)
{
    foreach ($form['fields'] as &$field) {

        // Only populate field ID 12
        if ($field['id'] == 22) {

            //  $get_post_id = "965643";
            $get_post_id = $_GET['id'];

            $flooringtype = get_post_type($get_post_id);
            $collection =  get_post_meta($get_post_id, 'collection', true);
            $satur = array('Masland', 'Dixie Home');

            if ($collection != NULL) {

                if ($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {

                    $familycolor = get_post_meta($get_post_id, 'style', true);
                    $key = 'style';
                } else {

                    $familycolor = $collection;
                    $key = 'collection';
                }
            } else {

                if (in_array(get_post_meta($get_post_id, 'brand', true), $satur)) {
                    $familycolor = get_post_meta($get_post_id, 'design', true);
                    $key = 'design';
                }
            }

            $args = array(
                'post_type'      => $flooringtype,
                'posts_per_page' => -1,
                'post_status'    => 'publish',
                'meta_query'     => array(
                    array(
                        'key'     => $key,
                        'value'   => $familycolor,
                        'compare' => '='
                    ),
                    array(
                        'key' => 'swatch_image_link',
                        'value' => '',
                        'compare' => '!='
                    )
                )
            );

            $the_query = new WP_Query($args);


            $choices = array(); // Set up blank array


            // loop over each select item at add value/option to $choices array

            while ($the_query->have_posts()) {
                $the_query->the_post();

                $image = swatch_image_product_thumbnail(get_the_ID(), '222', '222');

                $brand = get_post_meta(get_the_ID(), 'brand', true);
                $collection = get_post_meta(get_the_ID(), 'collection', true);
                $color = get_post_meta(get_the_ID(), 'color', true);

                $text = $brand . ' ' . $collection . ' ' . $color;

                $choices[] = array('text' =>  get_the_title(get_the_ID()), 'price' =>  $image, 'value' => get_the_title(get_the_ID()));
            }
            wp_reset_postdata();

            // Set placeholder text for dropdown
            $field->placeholder = '-- Choose color --';

            // Set choices from array of ACF values
            $field->choices = $choices;
        }
    }
    return $form;
}


// add_filter('facetwp_template_html', function ($output, $class) {
//     //$GLOBALS['wp_query'] = $class->query;
//     $prod_list = $class->query;

//     ob_start();
//     //    var_dump($class->query);
//     if (isset($prod_list->query['lighting_category'])) {

//         $dir = get_stylesheet_directory() . '/lighting-loop.php';
//         require_once $dir;
//     } elseif (get_option('plplayout') != '' && get_option('plplayout') == '1') {

//         $dir = WP_PLUGIN_DIR . '/grand-child/product-listing-templates/product-loop-popup.php';
//         require_once $dir;
//     } else {
//         $dir = WP_PLUGIN_DIR . '/grand-child/product-listing-templates/product-loop-new.php';
//         require_once $dir;
//     }

//     return ob_get_clean();
// }, 10, 2);

add_filter('single_template', 'childtheme_get_custom_post_type_template');

function childtheme_get_custom_post_type_template($single_template)
{
    global $post;

    if ($post->post_type != 'post') {

        // if ($post->post_type == 'carpeting') {

        //     $single_template = get_stylesheet_directory() . '/product-listing-templates/single-' . $post->post_type . '.php';
        // } else {

        $single_template = WP_PLUGIN_DIR . '/mm-retailer-plugin/product-listing-templates/single-' . $post->post_type . '.php';
        // }
    }
    return $single_template;
}




// IP location FUnctionality
if (!wp_next_scheduled('cde_preferred_location_cronjob')) {

    wp_schedule_event(time() +  17800, 'daily', 'cde_preferred_location_cronjob');
}

add_action('cde_preferred_location_cronjob', 'cde_preferred_location');

function cde_preferred_location()
{

    global $wpdb;

    if (! function_exists('post_exists')) {
        require_once(ABSPATH . 'wp-admin/includes/post.php');
    }

    //CALL Authentication API:
    $apiObj = new APICaller;
    $inputs = array('grant_type' => 'client_credentials', 'client_id' => get_option('CLIENT_CODE'), 'client_secret' => get_option('CLIENTSECRET'));
    $result = $apiObj->call(AUTHURL, "POST", $inputs, array(), AUTH_BASE_URL);


    if (isset($result['error'])) {
        $msg = $result['error'];
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] = $result['error_description'];
    } else if (isset($result['access_token'])) {

        //API Call for getting website INFO
        $inputs = array();
        $headers = array('authorization' => "bearer " . $result['access_token']);
        $website = $apiObj->call(BASEURL . get_option('SITE_CODE'), "GET", $inputs, $headers);

        // write_log($website['result']['locations']);

        for ($i = 0; $i < count($website['result']['locations']); $i++) {

            if ($website['result']['locations'][$i]['type'] == 'store' && $website['result']['locations'][$i]['address'] != null) {

                $location_name = isset($website['result']['locations'][$i]['city']) ? $website['result']['locations'][$i]['name'] : "";

                //  write_log($location_name);

                $found_post = post_exists($location_name, '', '', 'store-locations');

                // write_log('found_post----'.$found_post);

                if ($found_post == 0) {

                    $array = array(
                        'post_title' => $location_name,
                        'post_type' => 'store-locations',
                        'post_content'  => "",
                        'post_status'   => 'publish',
                        'post_author'   => 0,
                    );
                    $post_id = wp_insert_post($array);

                    //  write_log( $location_name.'---'.$post_id);

                    update_post_meta($post_id, 'address', $website['result']['locations'][$i]['address']);
                    update_post_meta($post_id, 'city', $website['result']['locations'][$i]['city']);
                    update_post_meta($post_id, 'state', $website['result']['locations'][$i]['state']);
                    update_post_meta($post_id, 'country', $website['result']['locations'][$i]['country']);
                    update_post_meta($post_id, 'postal_code', $website['result']['locations'][$i]['postalCode']);
                    if ($website['result']['locations'][$i]['forwardingPhone'] == '') {

                        update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['phone']);
                    } else {

                        update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['forwardingPhone']);
                    }

                    update_post_meta($post_id, 'latitude', $website['result']['locations'][$i]['lat']);
                    update_post_meta($post_id, 'longitue', $website['result']['locations'][$i]['lng']);
                    update_post_meta($post_id, 'monday', $website['result']['locations'][$i]['monday']);
                    update_post_meta($post_id, 'tuesday', $website['result']['locations'][$i]['tuesday']);
                    update_post_meta($post_id, 'wednesday', $website['result']['locations'][$i]['wednesday']);
                    update_post_meta($post_id, 'thursday', $website['result']['locations'][$i]['thursday']);
                    update_post_meta($post_id, 'friday', $website['result']['locations'][$i]['friday']);
                    update_post_meta($post_id, 'saturday', $website['result']['locations'][$i]['saturday']);
                    update_post_meta($post_id, 'sunday', $website['result']['locations'][$i]['sunday']);

                    $location_address_url =  "Color Interiors +";
                    $location_address_url  .= isset($website['result']['locations'][$i]['address']) ? $website['result']['locations'][$i]['address'] . " " : "";
                    $location_address_url .= isset($website['result']['locations'][$i]['city']) ? $website['result']['locations'][$i]['city'] . " " : "";
                    $location_address_url .= isset($website['result']['locations'][$i]['state']) ? $website['result']['locations'][$i]['state'] . " " : "";
                    $location_address_url .= isset($website['result']['locations'][$i]['postalCode']) ? $website['result']['locations'][$i]['postalCode'] . " " : "";


                    update_post_meta($post_id, 'map_url', "https://maps.google.com/maps/?q=" . urlencode($location_address_url) . "");
                } else {

                    update_post_meta($found_post, 'address', $website['result']['locations'][$i]['address']);
                    update_post_meta($found_post, 'city', $website['result']['locations'][$i]['city']);
                    update_post_meta($found_post, 'state', $website['result']['locations'][$i]['state']);
                    update_post_meta($found_post, 'country', $website['result']['locations'][$i]['country']);
                    update_post_meta($found_post, 'postal_code', $website['result']['locations'][$i]['postalCode']);
                    if ($website['result']['locations'][$i]['forwardingPhone'] == '') {

                        update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['phone']);
                    } else {

                        update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['forwardingPhone']);
                    }

                    update_post_meta($found_post, 'latitude', $website['result']['locations'][$i]['lat']);
                    update_post_meta($found_post, 'longitue', $website['result']['locations'][$i]['lng']);
                    update_post_meta($found_post, 'monday', $website['result']['locations'][$i]['monday']);
                    update_post_meta($found_post, 'tuesday', $website['result']['locations'][$i]['tuesday']);
                    update_post_meta($found_post, 'wednesday', $website['result']['locations'][$i]['wednesday']);
                    update_post_meta($found_post, 'thursday', $website['result']['locations'][$i]['thursday']);
                    update_post_meta($found_post, 'friday', $website['result']['locations'][$i]['friday']);
                    update_post_meta($found_post, 'saturday', $website['result']['locations'][$i]['saturday']);
                    update_post_meta($found_post, 'sunday', $website['result']['locations'][$i]['sunday']);
                }
            }
        }
    }
}



//get ipaddress of visitor

function getUserIpAddr()
{
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if (isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    if (strstr($ipaddress, ',')) {
        $tmp = explode(',', $ipaddress, 2);
        $ipaddress = trim($tmp[1]);
    }
    return $ipaddress;
}


// Custom function for lat and long

function get_storelisting()
{

    global $wpdb;
    $content = "";

    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';

    $response = wp_remote_post(
        $urllog,
        array(
            'method' => 'GET',
            'timeout' => 45,
            'redirection' => 5,
            'httpversion' => '1.0',
            'headers' => array('Content-Type' => 'application/json'),
            'body' =>  array('u' => 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0', 'ip' => getUserIpAddr(), 'dbs' => 'all', 'trans_id' => 'example', 'json' => 'true'),
            'blocking' => true,
            'cookies' => array()
        )
    );

    $rawdata = json_decode($response['body'], true);
    $userdata = $rawdata['response'];

    $autolat = $userdata['pulseplus-latitude'];
    $autolng = $userdata['pulseplus-longitude'];
    if (isset($_COOKIE['preferred_store'])) {

        $store_location = $_COOKIE['preferred_store'];
    } else {

        $store_location = '';
    }

    $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
              ( 3959 * acos( cos( radians(" . $autolat . " ) ) * cos( radians( post_lat.meta_value ) ) * 
              cos( radians( post_lng.meta_value ) - radians( " . $autolng . " ) ) + sin( radians( " . $autolat . " ) ) * 
              sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
              INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
              INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
              WHERE posts.post_type = 'store-locations' 
              AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";



    $storeposts = $wpdb->get_results($sql);

    // write_log($storeposts);

    $storeposts_array = json_decode(json_encode($storeposts), true);

    if ($store_location == '') {
        $store_location = $storeposts_array['0']['ID'];
    } else {
        $key = array_search($store_location, array_column($storeposts_array, 'ID'));
    }

    //$content = get_the_title($store_location); 
    $content = get_field('store_title', $store_location);
    $phone = get_field('phone', $store_location);


    foreach ($storeposts as $post) {


        $content_list .= '<div class="store_wrapper " id ="' . $post->ID . '">
                	<h5 class="title-prefix-fix">COLOR INTERIORS</h5>
                    <h5 class="title-prefix">' . get_field('store_title',  $post->ID) . '</h5>
                    <h5 class="store-add"> ' . get_field('address', $post->ID) . '<br />' . get_field('city', $post->ID) . ', ' . get_field('state', $post->ID) . ' ' . get_field('postal_code', $post->ID) . '</h5>';



        if (get_field(strtolower(date("l")), $post->ID) == 'CLOSED') {

            $content_list .= '<p class="store-hour">CLOSED TODAY</p>';
        } else {

            $openuntil = explode("-", get_field(strtolower(date("l")), $post->ID));

            $content_list .= '<p class="store-hour">OPEN UNTIL ' . str_replace(' ', '', $openuntil[1]) . '</p>';
        }


        $content_list .= '<p class="store-phone"><a href="tel:' . get_field('phone', $post->ID) . '">' . get_field('phone', $post->ID) . '</a> </p>';


        $content_list .= '<a href="' . get_field('map_url', $post->ID) . '" target="_blank" class="store-cta-link view_location"> GET DIRECTION </a>';

        $content_list .= '<a href="' . get_field('store_url', $post->ID) . '" data-id="' . $post->ID . '" data-storename="' . get_the_title($post->ID) . '" data-distance="' . round($post->distance, 1) . '" data-phone="' . get_field('phone', $post->ID) . '" target="_self" data-link="' . get_field('store_url', $post->ID) . '" class="store-cta-link more">LEARN MORE  </a>';
        $content_list .= '<a data-id="' . $post->ID . '" data-storename="' . get_the_title($post->ID) . '" data-distance="' . round($post->distance, 1) . '" data-phone="' . get_field('phone', $post->ID) . '" target="_self" data-link="' . get_field('store_url', $post->ID) . '" data-storetitle="' . get_field('store_title',  $post->ID) . '" class="store-cta-link choose_location">CHOOSE LOCATION <input type="checkbox" class="currentLoc" style="visibility: hidden;" /> </a>';


        $content_list .= '</div>';
    }


    $data = array();

    $data['header'] = $content;
    $data['phone'] = $phone;
    $data['list'] = $content_list;

    echo json_encode($data);
    wp_die();
}


add_action('wp_ajax_nopriv_get_storelisting', 'get_storelisting', '1');
add_action('wp_ajax_get_storelisting', 'get_storelisting', '1');



//choose this location FUnctionality

add_action('wp_ajax_nopriv_choose_location', 'choose_location');
add_action('wp_ajax_choose_location', 'choose_location');

function choose_location()
{
    //$storeN = get_the_title($_POST['store_id']); 
    $storeN = get_field('store_title', $_POST['store_id']);
    $content .= '<p>You’re Shopping</p>
      <h3 class="header_location_name">' .  $storeN . '</h3>
	 <div class="phone"><a class="mobile_phone" href="tel:' . $_POST['phone'] . '">' . $_POST['phone'] . '</a></div>	
      <div class="storeLink"><a href="/who-we-are/locations/">View All Locations<br></a></div>';

    echo $content;

    wp_die();
}



add_filter('gform_pre_render_28', 'populate_product_location_form');
add_filter('gform_pre_validation_28', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_28', 'populate_product_location_form');
add_filter('gform_admin_pre_render_28', 'populate_product_location_form');

add_filter('gform_pre_render_19', 'populate_product_location_form');
add_filter('gform_pre_validation_19', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_19', 'populate_product_location_form');
add_filter('gform_admin_pre_render_19', 'populate_product_location_form');

add_filter('gform_pre_render_7', 'populate_product_location_form');
add_filter('gform_pre_validation_7', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_7', 'populate_product_location_form');
add_filter('gform_admin_pre_render_7', 'populate_product_location_form');

add_filter('gform_pre_render_20', 'populate_product_location_form');
add_filter('gform_pre_validation_20', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_20', 'populate_product_location_form');
add_filter('gform_admin_pre_render_20', 'populate_product_location_form');

add_filter('gform_pre_render_30', 'populate_product_location_form');
add_filter('gform_pre_validation_30', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_30', 'populate_product_location_form');
add_filter('gform_admin_pre_render_30', 'populate_product_location_form');

add_filter('gform_pre_render_10', 'populate_product_location_form');
add_filter('gform_pre_validation_10', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_10', 'populate_product_location_form');
add_filter('gform_admin_pre_render_10', 'populate_product_location_form');

add_filter('gform_pre_render_24', 'populate_product_location_form');
add_filter('gform_pre_validation_24', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_24', 'populate_product_location_form');
add_filter('gform_admin_pre_render_24', 'populate_product_location_form');

add_filter('gform_pre_render_5', 'populate_product_location_form');
add_filter('gform_pre_validation_5', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_5', 'populate_product_location_form');
add_filter('gform_admin_pre_render_5', 'populate_product_location_form');

add_filter('gform_pre_render_9', 'populate_product_location_form');
add_filter('gform_pre_validation_9', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_9', 'populate_product_location_form');
add_filter('gform_admin_pre_render_9', 'populate_product_location_form');

add_filter('gform_pre_render_26', 'populate_product_location_form');
add_filter('gform_pre_validation_26', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_26', 'populate_product_location_form');
add_filter('gform_admin_pre_render_26', 'populate_product_location_form');

add_filter('gform_pre_render_33', 'populate_product_location_form');
add_filter('gform_pre_validation_33', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_33', 'populate_product_location_form');
add_filter('gform_admin_pre_render_33', 'populate_product_location_form');


add_filter('gform_pre_render_31', 'populate_product_location_form');
add_filter('gform_pre_validation_31', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_31', 'populate_product_location_form');
add_filter('gform_admin_pre_render_31', 'populate_product_location_form');

add_filter('gform_pre_render_15', 'populate_product_location_form');
add_filter('gform_pre_validation_15', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_15', 'populate_product_location_form');
add_filter('gform_admin_pre_render_15', 'populate_product_location_form');

add_filter('gform_pre_render_13', 'populate_product_location_form');
add_filter('gform_pre_validation_13', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_13', 'populate_product_location_form');
add_filter('gform_admin_pre_render_13', 'populate_product_location_form');


function populate_product_location_form($form) {

    foreach ($form['fields'] as &$field) {

        // Only populate field ID 12
        if ($field->type != 'select' || strpos($field->cssClass, 'populate-store') === false) {
            continue;
        }

        $args = array(
            'post_type'      => 'store-locations',
            'posts_per_page' => -1,
            'post_status'    => 'publish'
        );

        $locations =  get_posts($args);

        $choices = array();

        foreach ($locations as $location) {

            $title = get_field('store_title', $location->ID);

            $choices[] = array('text' => $title, 'value' => $title);
        }

        wp_reset_postdata();

        $field->placeholder = 'Preferred Location';

        $field->choices = $choices;
    }

    return $form;
}


/* preload images for page speed optimization */
function saleslideBgimages($arg){

    ob_start();

    if ( is_front_page() ) { 
    $seleinformation = json_decode(get_option('salesliderinformation'));

    if(isset($seleinformation )){

    usort($seleinformation, 'compare_cde_some_objects');        
    }

    $website_json =  json_decode(get_option('website_json'));
    

    if(isset($seleinformation)){  
        
        $i = 0;
        foreach($seleinformation as $slide){
            $slider_bg_img = $slide->slider->slider_bg_img;
            
            
            if($i == 0){

if(preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo 
|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i" 
, $_SERVER["HTTP_USER_AGENT"])){
                echo '<link rel="preload" class="shortCUt" fetchpriority="high" href="https://mm-media-res.cloudinary.com/image/fetch/c_crop,h_768,w_768/'.$slider_bg_img.'" as="image" />';

    
            }else { 
                echo '<link rel="preload" class="shortCUt" fetchpriority="high" href="'.$slider_bg_img.'" as="image" />';
            }

            }
           
              $i++; 
          
        }
       
    }  
    return $link;
    }

}
add_action('wp_head', 'saleslideBgimages', 0);

