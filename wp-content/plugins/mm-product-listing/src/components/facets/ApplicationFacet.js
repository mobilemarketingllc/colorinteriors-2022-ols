import React from "react";

export default function ApplicationFacet({
  handleFilterClick,
  productApplications,
}) {
  function sortObject(obj) {
    return Object.keys(obj)
      .sort()
      .reduce((a, v) => {
        a[v] = obj[v];
        return a;
      }, {});
  }
  productApplications = sortObject(productApplications);

  return (
    <div class="facet-wrap facet-display">
      <strong>Application</strong>
      <div className="facetwp-facet">
        {Object.keys(productApplications).map((application, i) => {
          if (application && productApplications[application] > 0) {
            return (
              <div>
                <span
                  id={`application-filter-${i}`}
                  key={i}
                  data-value={`${application.toLowerCase()}`}
                  onClick={(e) =>
                    handleFilterClick(
                      "application_facet",
                      e.target.dataset.value
                    )
                  }>
                  {" "}
                  {application} {` (${productApplications[application]}) `}
                </span>
              </div>
            );
          }
        })}
      </div>
    </div>
  );
}
